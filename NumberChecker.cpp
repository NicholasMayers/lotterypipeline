#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <queue>
#include <sstream>
#include <cstring>
#include <string>

using namespace std;

queue<string> list_dir(const char *path)
{
    struct dirent *entry;
    queue<string> files;
    DIR *dir = opendir(path);

    if (dir == NULL)
    {
        return(files);
    }
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            string name = entry->d_name;
            int position = name.find_last_of(".");
            string fileType = name.substr(position, 4);

            if (fileType == ".csv")
                files.push(entry->d_name);
        }
        
    }
    closedir(dir);

    return (files);
}

void CheckTicket(string fileName, int winningNum[], const char *path)
{
    ifstream ticketFile;
    ticketFile.open(path + fileName);
    string name = fileName.substr(0, fileName.length() - 4);

    string ticketStr;
    getline(ticketFile, ticketStr);
    stringstream ss(ticketStr);

    char tmp;
    int val = 0;
    int ticket[6];
    int i = 0;
    while (ss >> tmp)
    {
        if (i > 5)
            break;

        if (isdigit(tmp))
        {
            val = val * 10 + (tmp - '0');
        }
        else
        {
            ticket[i] = val;
            val = 0;
            i++;
        }
    }
    if (val > 0 && i < 6)
    {
        ticket[i] = val;
        i++;
    }
        
    int sameNumbers = 0;
    for (long unsigned int i = 0; i < 6; i++)
    {
        for (long unsigned int ij = 0; ij < 6; ij++)
        {
            if (winningNum[i] == ticket[ij])
                sameNumbers++;
        }
    }

    if (sameNumbers > 2)
    {
        printf("%s has %i of the winning numbers!\n", name.c_str(), sameNumbers);
    }
}

int main(int argc, char *argv[])
{
    int winningNum[6];

    for (long unsigned int i = 0; i < 6; i++)
    {
        scanf("%d", &winningNum[i]);
    }

    const char *path = "LotteryTickets/";
    queue<string> lotteryTickets = list_dir(path);

    if (lotteryTickets.empty())
    {
        printf("No CSV files found!\n");
        return (0);
    }

    while (!lotteryTickets.empty())
    {
        CheckTicket(lotteryTickets.front(), winningNum, path);
        lotteryTickets.pop();
    }

    return (0);
}

