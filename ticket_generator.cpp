#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int randomNum;
    FILE *lotteryNumFile;
    string names [] = { "Anita", "Gavin", "Katelin", "Jerome", "Renie", "Josephina", "Driskoll", "Natille", "Jeanna", "Wayland","Ada","Ernest", "Ida", "Donald", "Sofia", "Calvin", "Antonia", "Roberto", "Edward", "Lloyd", "Leanne", "Daryl", "Daniel", "Alexander", "Ramesh", "Julia", "Nancy", "Amy", "Carole", "Kerrie", "Kiran", "Winifred", "Bradley", "Karolina", "Barbara", "Anthea", "Gareth"};

    srand(time(NULL));

    for (long unsigned int i = 0; i < sizeof(names); i++)
    {
        string fileName = "LotteryTickets/" + names[i] + ".csv";
        lotteryNumFile = fopen(fileName.c_str(), "w");
        vector<int> lotteryNum;
        lotteryNum.empty();
        lotteryNum.reserve(6);

        for (long unsigned int ij = 0; ij <= 5;)
        {
            bool found = false;
            randomNum = rand() % 60;

            for (long unsigned int ijk = 0; ijk < lotteryNum.size(); ijk++)
            {
                if (randomNum == lotteryNum[ijk])
                {
                    found = true;
                }
            }
            
            if (found != true)
            {
                lotteryNum.push_back(randomNum);
                ij++;
            }
        }

        for (long unsigned int i = 0; i < lotteryNum.size() -1; i++)
            fprintf(lotteryNumFile, "%d,", lotteryNum[i]);

        fprintf(lotteryNumFile, "%d", lotteryNum.front());

        fclose(lotteryNumFile);
    }

    return (0);
} 